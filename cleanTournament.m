%% Clean workspace, run-folders and log-related files and folders.

playersFolder = 'players';
playerName = 'GLaDOS';
enemyFolder = 'test_enemys';
logfileWildcard = 'tournament_main_log_*';
logFolder = 'logs';

%% removing old "build"/"run" folder
if exist(playersFolder, 'dir')
    rmdir(playersFolder, 's');
end

%% create archive log folder, if it doesn't exist already
if ~exist(logFolder, 'dir')
    mkdir(logFolder);
end

%% removing all log-related files and folders
try
    movefile(logfileWildcard, logFolder);
catch
    % no directory
end

clear all; close all; %#ok<CLSCR>