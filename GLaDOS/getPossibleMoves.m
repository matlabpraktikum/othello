function moves = getPossibleMoves( board, color, isLeaf )
%% GETPOSSIBLEMOVES returns all possible moves
% returns an 3d-matrix of child boards for the current board and color.

free_spots = board == 0;

mask = zeros(10);
mask(2:9,2:9) = board;
spots = find(mask == -color);
spots = spots(:,ones(1,8));
neighbourIdx = [-11,-10,-9,-1,1,9,10,11];
neighbourIdx = neighbourIdx(ones(size(spots,1),1),:);
spots = spots - neighbourIdx;
spots = spots(:);
mask(spots) = 1;
neighbor_spots = mask(2:9,2:9);

movesBoard = free_spots & neighbor_spots;
[row,col] = find(movesBoard);

moves = zeros(8,8,length(row));
scores = zeros(length(row),1);

for i=1:length(row)
    [scores(i), moves(:,:,i)] = getScoreForCandidate(row(i), col(i), board, color);
end

moves(:,:,scores==0) = [];

if (size(moves,3) == 0 && ~isLeaf)
    if (size(getPossibleMoves(board, -color, true),3) > 0)
        moves = board;
    end
end

end

function [score,resultingBoard] = getScoreForCandidate(row, col, board, color)
score = 0;

dir = [-1 -1; -1 0; -1 1; 0 1; 1 1;  1 0; 1 -1; 0 -1];
dirIdx = dir * [1;8];
candidate = [row; col];
candidateIdx = row + (col-1) * 8;
ownBoard = color * board;
resultingBoard = board;

for d=1:8
    step = 1;
    possiblemove = false;
    flipIdx = zeros(8,1);
    while 1
        nextRow = candidate(1) + step * dir(d,1);
        nextCol = candidate(2) + step * dir(d,2);
        
        nextIdx = candidateIdx + step * dirIdx(d);
        
        %check if boarder is reached
        if (nextRow == 0 || nextCol == 0 || nextRow == 9 || nextCol == 9)
            break;
        end
        nextCoin = ownBoard(nextIdx);

        %check for own token
        if nextCoin == 1
            if step == 1
                break;
            else
                flipIdx = flipIdx(1:step-1);
                resultingBoard(flipIdx) = color;
                possiblemove = true;
                break;
            end
        end

        %check for enemy token
        if nextCoin == 0
            break;
        else
            flipIdx(step) = nextIdx;
        end

        step = step + 1;
    end
    if possiblemove
        score = 1;
        resultingBoard(candidateIdx) = color;
    end
end
end