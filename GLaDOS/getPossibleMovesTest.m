function getPossibleMovesTest()
%% GETPOSSIBLEMOVESTEST Testfunction of getPossibleMoves

disp(' ')
display([mfilename,':'])
getPossibleMoves_startBoardAndBlack_correct();
getPossibleMoves_startBoardAndWhite_correct();
getPossibleMoves_startBoardWithFirstMoveAndBlack_correct();
getPossibleMoves_startBoardWithFirstMoveAndWhite_correct();
getPossibleMoves_startBoardWithFirstFlippedMoveAndWhite_correct();
getPossibleMoves_startBoardWithFirstMove2AndWhite_correct();
getPossibleMoves_noValidMovesButOtherHas_movesSizeGreater0();
getPossibleMoves_boardFull_movesSize0();
getPossibleMoves_noValidMovesForBoth_movesSize0();
getPossibleMoves_finalPhaseMatch_performanceTest();

end

function validSpots = getValidSpotsFromMoves(moves, originalBoard)

numberOfMoves = size(moves,3);
newMoves = moves;

for i = 1:numberOfMoves
    currentMove = moves(:,:,i);
    newMoves(:,:,i) = (currentMove ~= 0) & (originalBoard==0);
end

validSpots = sum(newMoves,3);

end

function getPossibleMoves_startBoardAndBlack_correct()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
color = -1;

tic;
moves = getPossibleMoves(board, color, false);
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = [0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0;0 0 0 1 0 0 0 0;0 0 1 0 0 0 0 0;0 0 0 0 0 1 0 0;0 0 0 0 1 0 0 0;0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0];
executeTestAssertEqual('getPossibleMoves_startBoardAndBlack_correct', result, shouldBe, time);

end

function getPossibleMoves_startBoardAndWhite_correct()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
color = 1;

tic;
moves = getPossibleMoves(board, color, false); 
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = [0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0;0 0 0 1 0 0 0 0;0 0 1 0 0 0 0 0;0 0 0 0 0 1 0 0;0 0 0 0 1 0 0 0;0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0];
shouldBe = shouldBe(end:-1:1,:);
executeTestAssertEqual('getPossibleMoves_startBoardAndWhite_correct', result, shouldBe, time);

end

function getPossibleMoves_startBoardWithFirstMoveAndBlack_correct()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = 1;
board(3,5) = 1;
board(5,4) = -1;
color = -1;

tic;
moves = getPossibleMoves(board, color, false); 
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = zeros(8);
shouldBe(3,4) = 1;
shouldBe(3,6) = 1;
shouldBe(5,6) = 1;
executeTestAssertEqual('getPossibleMoves_startBoardWithFirstMoveAndBlack_correct', result, shouldBe, time);

end

function getPossibleMoves_startBoardWithFirstMoveAndWhite_correct()

board = zeros(8);
board(3,4) = -1;
board(4,4) = -1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
color = 1;

tic;
moves = getPossibleMoves(board, color, false); 
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = zeros(8);
shouldBe(3,5) = 1;
shouldBe(5,3) = 1;
shouldBe(3,3) = 1;
executeTestAssertEqual('getPossibleMoves_startBoardWithFirstMoveAndWhite_correct', result, shouldBe, time);

end

function getPossibleMoves_startBoardWithFirstFlippedMoveAndWhite_correct()

board = zeros(8);
board(3,4) = -1;
board(4,4) = -1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
board = board(end:-1:1,:);
color = 1;

tic;
moves = getPossibleMoves(board, color, false); 
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = zeros(8);
shouldBe(3,5) = 1;
shouldBe(5,3) = 1;
shouldBe(3,3) = 1;
shouldBe = shouldBe(end:-1:1,:);
executeTestAssertEqual('getPossibleMoves_startBoardWithFirstFlippedMoveAndWhite_correct', result, shouldBe, time);

end

function getPossibleMoves_startBoardWithFirstMove2AndWhite_correct()

board = zeros(8);
board(3,4) = -1;
board(4,4) = -1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
board = board(end:-1:1,end:-1:1);
color = 1;

tic;
moves = getPossibleMoves(board, color, false); 
time = toc;
result = getValidSpotsFromMoves(moves, board);
shouldBe = zeros(8);
shouldBe(3,5) = 1;
shouldBe(5,3) = 1;
shouldBe(3,3) = 1;
shouldBe = shouldBe(end:-1:1,end:-1:1);
executeTestAssertEqual('getPossibleMoves_startBoardWithFirstMove2AndWhite_correct', result, shouldBe, time);

end

function getPossibleMoves_noValidMovesButOtherHas_movesSizeGreater0()

board = ones(8);
board(1:8) = 0;
board(11) = -1;
color = -1;
tic;
moves = getPossibleMoves(board, color, false); 
time = toc;

result = size(moves,3);
executeTestAssert('getPossibleMoves_noValidMovesButOtherHas_movesSizeGreater0', result>0, time);

end

function getPossibleMoves_boardFull_movesSize0()

board = ones(8);
color = 1;
tic;
moves = getPossibleMoves(board, color, false); 
time = toc;

result = size(moves,3);
executeTestAssertEqual('getPossibleMoves_boardFull_movesSize0', result, 0, time);

end

function getPossibleMoves_noValidMovesForBoth_movesSize0()

board = ones(8);
board(1:3) = 0;
color = 1;
tic;
moves = getPossibleMoves(board, color, false); 
time = toc;

result = size(moves,3);
executeTestAssertEqual('getPossibleMoves_noValidMovesForBoth_movesSize0', result, 0, time);

end

function getPossibleMoves_finalPhaseMatch_performanceTest()
board = [...
 1, 1, 1, 1, 1, 1, 0, 0;
 1, 1,-1,-1,-1,-1, 0, 0;
 0,-1, 1,-1,-1,-1,-1, 0;
-1,-1, 1, 1,-1,-1,-1,-1;
-1,-1,-1, 1, 1, 1,-1, 0;
-1,-1, 1,-1, 1, 1,-1, 0;
 0, 1, 1, 1, 1, 1, 1, 0;
 1,-1,-1,-1,-1,-1, 0, 0];
color = 1;

tic;
for i = 1:100
    getPossibleMoves(board, color, false);
end
toc;

end