function executeTestAssert( functionName, result, time )
%% EXECUTETESTASSERT Checks if statement is true

if (logical(result))
    display([functionName, ' is correct and runs ', num2str(time), ' seconds.']);
else
    warning([functionName, ' is not correct']);
end

end

