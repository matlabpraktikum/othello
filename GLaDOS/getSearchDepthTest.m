function getSearchDepthTest()
%% GETSEARCHDEPTHTEST Testfunction of getSearchDepth

disp(' ')
display([mfilename,':'])

getSearchDepth_beginningOfMatchTest();
getSearchDepth_seeTheEnd_depthIsBig();
getSearchDepth_normalPlayMode_depthIsNormal();

end

function getSearchDepth_beginningOfMatchTest()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;

tic;
depth = getSearchDepth(board,170.0);
time = toc;

executeTestAssert('getSearchDepth_beginningOfMatchTest', depth > 3,time);

end

function getSearchDepth_seeTheEnd_depthIsBig()

board = ones(8);
board(1:3:30) = 0;

tic;
depth = getSearchDepth(board,170.0);
time = toc;

executeTestAssert('getSearchDepth_seeTheEnd_depthIsBig', depth > 100,time);

end

function getSearchDepth_normalPlayMode_depthIsNormal()

board = ones(8);
board(1:2:30) = 0;

tic;
depth = getSearchDepth(board,170.0);
time = toc;

executeTestAssert('getSearchDepth_normalPlayMode_depthIsNormal', depth < 10,time);

end