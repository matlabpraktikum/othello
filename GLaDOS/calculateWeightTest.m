function calculateWeightTest()
%% CALCULATEWEIGHTTEST Testfunction of calculateWeight

disp(' ')
display([mfilename,':'])
calculateWeight_startBoardAndWhite_correct;
calculateWeight_startBoardWithirstMoveAndWhite_correct;
calculateWeight_randomBoardWithOneCorner_correct;
calculateWeight_randomBoardWithOneNextToCorner_correct;

end

function calculateWeight_startBoardAndWhite_correct()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
color = 1;

tic;
result = calculateWeight(board, color); 
time = toc;
shouldBe = 0;
executeTestAssertEqual('calculateWeight_startBoardAndWhite_correct', result, shouldBe, time);

end

function calculateWeight_startBoardWithirstMoveAndWhite_correct()

board = zeros(8);
board(3,4) = -1;
board(4,4) = -1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;
color = 1;

tic;
result = calculateWeight(board, color); 
time = toc;
shouldBe = 5.19168e+03;
executeTestAssertEqual('calculateWeight_startBoardWithirstMoveAndWhite_correct', result, shouldBe, time);

end

function calculateWeight_randomBoardWithOneCorner_correct()

board = zeros(8);
board(1,1) = -1;
color = 1;

tic;
result = calculateWeight(board, color); 
time = toc;
shouldBe = -1.38035e+04;
executeTestAssertEqual('calculateWeight_randomBoardWithOneCorner_correct', result, shouldBe, time);

end

function calculateWeight_randomBoardWithOneNextToCorner_correct()

board = zeros(8);
board(1,2) = -1;
color = 1;

tic;
result = calculateWeight(board, color); 
time = toc;
shouldBe = 1.1244925e+04;
executeTestAssertEqual('calculateWeight_randomBoardWithOneNextToCorner_correct', result, shouldBe, time);

end