function minimaxTest()
%% MINIMAXTEST Testfunction of minimax

disp(' ')
display([mfilename,':'])
minimax_startBoardAndBlack_validMove();

end

function minimax_startBoardAndBlack_validMove()

board = zeros(8);
board(4,4) = 1;
board(5,5) = 1;
board(4,5) = -1;
board(5,4) = -1;

color = -1;
depth = 5;

tic;
minimax(color, board, depth, -inf, inf);
toc;

end