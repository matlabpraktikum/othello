function board = calculateNextMove(board, color, t)
%% CALCULATENEXTMOVE Start point for the internal calculation
% returns the calculated board for the next move.

depth = getSearchDepth(board, t);
[~, turn] = minimax(color, board, depth, -inf, inf);

if ~isempty(turn)
    board = turn;
end
end

