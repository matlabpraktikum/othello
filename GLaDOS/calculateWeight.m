function weight = calculateWeight(board, color)
%% CALCULATEWEIGHT returns weight for a color (for negamax -> max is good for color)
% Weight factors: 
% pieceDiff, cornerO, cornerC, mobility, frontier, diskSquares.

ownFrontTiles = 0;
oppFrontTiles = 0;

% weighted board
V = [20, -3, 11, 8, 8, 11, -3, 20; ...
-3, -7, -4, 1, 1, -4, -7, -3; ...
11, -4, 2, 2, 2, 2, -4, 11; ...
8, 1, 2, -3, -3, 2, 1, 8; ...
8, 1, 2, -3, -3, 2, 1, 8; ...
11, -4, 2, 2, 2, 2, -4, 11; ...
-3, -7, -4, 1, 1, -4, -7, -3; ...
20, -3, 11, 8, 8, 11, -3, 20];

%% Piece difference, frontier disks and disk squares
ownCoins = board == color;
ownTiles = sum(sum(ownCoins));
oppCoins = board == -color;
oppTiles = sum(sum(oppCoins));
diskSquares = sum(sum(V.*ownCoins)) - sum(sum(V.*oppCoins));

[row,col] = find(board ~= 0);
mask = ones(10);
mask(2:9,2:9) = board;
row = row + 1;
col = col + 1;
for i=1:length(row)
    if(any(any(mask(row(i)-1:row(i)+1,col(i)-1:col(i)+1) == 0)))
        if(mask(row(i),col(i)) == color)
            ownFrontTiles = ownFrontTiles + 1;
        else
            oppFrontTiles = oppFrontTiles + 1;
        end
    end
end

if(ownTiles > oppTiles)
    pieceDiff = (100.0 * ownTiles)/(ownTiles + oppTiles);
elseif(ownTiles < oppTiles)
    pieceDiff = -(100.0 * oppTiles)/(ownTiles + oppTiles);
else
    pieceDiff = 0;
end
if(ownFrontTiles > oppFrontTiles)
    frontier = -(100.0 * ownFrontTiles)/(ownFrontTiles + oppFrontTiles);
elseif(ownFrontTiles < oppFrontTiles)
    frontier = (100.0 * oppFrontTiles)/(ownFrontTiles + oppFrontTiles);
else
    frontier = 0;
end

%% Corner occupancy
cornerIndices = [1,8,57,64];
cornerTiles = board(cornerIndices);
ownTiles = sum(cornerTiles == color);
oppTiles = sum(cornerTiles == -color);
cornerO = 25 * (ownTiles - oppTiles);

%% Corner closeness
ownTiles = 0;
oppTiles = 0;
nearCornerIndices = [2,9,10;7,15,16;41,42,50;47,48,55];
for i=1:4
    if cornerTiles(i) == 0
        ownTiles = ownTiles + sum(board(nearCornerIndices(i,:)) == color);
        oppTiles = oppTiles + sum(board(nearCornerIndices(i,:)) == -color);
    end
end
cornerC = -12.5 * (ownTiles - oppTiles);

%% Mobility
ownTiles = size(getPossibleMoves(board, color, false),3);
oppTiles = size(getPossibleMoves(board, -color, false),3);
if(ownTiles > oppTiles)
    mobility = (100.0 * ownTiles)/(ownTiles + oppTiles);
elseif(ownTiles < oppTiles)
    mobility = -(100.0 * oppTiles)/(ownTiles + oppTiles);
else
    mobility = 0;
end

weight = (10 * pieceDiff) + (801.724 * cornerO) + (382.026 * cornerC) + (78.922 * mobility) + (74.396 * frontier) + (10 * diskSquares);

end
