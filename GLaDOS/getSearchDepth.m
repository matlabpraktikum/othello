function depth = getSearchDepth(board, timeLeft)
%% GETSEARCHDEPTH Decides how many layers should be considered by our minimax
% algorithm (based on board and time)

%% Emergency Modes
if timeLeft < 5
    depth = 2;
    return;
elseif timeLeft < 15
    depth = 3;
    return;
end;

%% Match Beginning or End?
emptyTiles = sum(sum(board==0));
if emptyTiles > 55
    depth = 5;
    return;
elseif emptyTiles <= 10
% tree should calculate until we reach the terminal node.
    depth = inf;
    return;
elseif emptyTiles <= 15
    depth = 6;
    return;
end

%% Mid Match Default Search Depth
depth = 4;
end

