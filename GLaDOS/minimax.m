function [maxValue, turn] = minimax(color, board, depth, alpha, beta)
%% MINIMAX Algorithm with alpha-beta pruning
% Wikipedia's negamax pseudocode implementation.

turn = [];

if depth == 0
    maxValue = calculateWeight(board,color);
    return;
end

moves = getPossibleMoves(board, color, false);
maxValue = alpha;

% if the current node is a terminal node we use the max score.
if size(moves,3) == 0
    maxValue = sum(color*board(:));
    return;
end

for i = 1:size(moves,3)
    moveBoard = moves(:,:,i);
    [moveValue, ~] = minimax(-color, moveBoard, depth-1, -beta, -maxValue);
    moveValue = -moveValue;
    if moveValue > maxValue
        maxValue = moveValue;
        turn = moveBoard;
        if (maxValue >= beta)
            break;
        end
    end
end
end