function executeTestPerformance( functionName, time )
%% EXECUTETESTPERFORMANCE Displays runtime and function name.

display([functionName, ' runs ', num2str(time), ' seconds.']);

end

