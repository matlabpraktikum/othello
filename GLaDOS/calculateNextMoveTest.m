function calculateNextMoveTest()
%% CALCULATENEXTMOVETEST Testfunction of calculateNextMove

disp(' ')
display([mfilename,':'])

calculateNextMove_performanceMidOfMatchTest();
calculateNextMove_endOfMatchTest();
calculateNextMove_endOfMatch2Test();
calculateNextMove_performanceEndOfMatchTest();
calculateNextMove_performanceBeforeEndOfMatchTest();
calculateNextMove_performanceBeforeEndOfMatchTest1();
calculateNextMove_performanceBeforeEndOfMatchTest2();

end

function calculateNextMove_performanceMidOfMatchTest()
board = [...
 0, 0, 1, 0, 1, 0, 0,0;
 0,-1,-1,-1,-1,-1, 0,0;
 0, 0, 1, 0,-1, 0, 0,0;
-1, 1,-1,-1, 1, 0, 0,0;
 1, 0,-1,-1,-1,-1, 0,0;
 0, 0, 1, 0,-1, 0, 0,0;
 0, 0, 0, 0, 0,-1, 0,0;
 0, 0, 0, 0, 0, 0,-1,0];

color = -1;

tic;
calculateNextMove(board,color,170.0);
time = toc;

executeTestPerformance('calculateNextMove_performanceMidOfMatchTest', time);

end

function calculateNextMove_endOfMatchTest()
board = [...
-1, 0, 1, 1, 1, 1, 1,0;
-1,-1,-1,-1,-1,-1,-1,1;
-1,-1,-1,-1,-1,-1,-1,1;
-1,-1,-1,-1,-1, 1,-1,1;
-1,-1,-1,-1, 1, 1,-1,1;
-1, 1,-1, 1, 1, 1, 1,1;
 1, 1, 1, 1, 1, 1, 0,1;
-1, 0, 1, 1, 1, 1, 1,0];

color = 1;

tic;
turn = calculateNextMove(board,color,170.0);
time = toc;

testIfExactlyOneStoneIsAdded = turn(board == 0 & turn ~=0);
executeTestAssertEqual('calculateNextMove_endOfMatchTest', testIfExactlyOneStoneIsAdded, color,time);
executeTestAssertEqual('calculateNextMove_endOfMatchTest', numel(testIfExactlyOneStoneIsAdded), 1,time);

end

function calculateNextMove_endOfMatch2Test()
board = ...
[-1, 1, 1, 1, 1, 1, 1, 1;
-1, 1, 1,-1,-1,-1, 1, 1;
-1, 1,-1, 1,-1, 1,-1, 1;
-1, 1,-1,-1, 1,-1,-1, 1;
-1, 1,-1,-1,-1, 1,-1, 1;
-1, 1,-1,-1, 1, 1, 1, 1;
 1, 1,-1, 1, 1, 1, 0, 1;
-1,-1,-1,-1,-1,-1,-1,-1];

expectedResult = board;
color = 1;

tic;
turn = calculateNextMove(board,color,170.0);
time = toc;

executeTestAssertEqual('calculateNextMove_endOfMatch2Test', turn, expectedResult,time);
end

function calculateNextMove_performanceEndOfMatchTest()
board = [1,-1, 1, 1, 1, 1, 0, 0;1,-1,-1, 1, 1, 1, 0,-1;1,-1, 1,-1, 1, 1, 1,-1;1, 1, 1, 1,-1,-1, 1,-1;1, 1, 1, 1,-1,-1,-1,-1;1, 1, 1, 1, 1,-1,-1,-1;0, 0, 1, 1,-1, 0, 0,-1;0, 0, 1, 1, 1, 1, 0, 0];
color = -1;

tic;
calculateNextMove(board,color,170.0);
time = toc;

executeTestPerformance('calculateNextMove_performanceEndOfMatchTest',time);

end

function calculateNextMove_performanceBeforeEndOfMatchTest()
board = [1 -1 1 1 1 1, 0, 0;1 -1 1 1 1 1, 0,-1;1 -1 1 1 -1 -1, -1, 0;1 -1 1 1 1 -1, 1, 1;1 1 -1 1 -1 1, 1, 0;1 1 1 -1 -1 -1, 1, -1;0 0 -1 -1 -1 0, 0, 0;0 0 0 -1 0 0, 0, 0];
color = -1;

tic;
calculateNextMove(board,color,170.0);
time = toc;

executeTestPerformance('calculateNextMove_performanceBeforeEndOfMatchTest',time);

end

function calculateNextMove_performanceBeforeEndOfMatchTest1()
board = [1,-1, 1, 1, 1, 1, 0, 0;1,-1,-1, 1, 1, 1, 0,-1;1,-1, 1,-1, 1, 1, 1,-1;1, 1, 1, 1,-1,-1, 1,-1;1, 1, 1, 1,-1,-1,-1,-1;1, 1, 1, 1, 1,-1,-1,-1;0, 0, 1, 1,-1, 0, 0,-1;0, 0, 1, 1, 1, 1, 0, 0];
color = -1;

tic;
calculateNextMove(board,color,170.0);
time = toc;

executeTestPerformance('calculateNextMove_performanceBeforeEndOfMatchTest1',time);

end

function calculateNextMove_performanceBeforeEndOfMatchTest2()
board = [-1, 1,-1,-1,-1,-1, 0, 0;-1,-1,-1, 1, 1, 1, 0,-1;-1,-1, 1, 1,-1,-1,-1,-1;-1,-1, 1,-1,-1,-1, 1,-1; 1, 1,-1,-1,-1,-1,-1,-1; 1, 0, 1, 1,-1,-1, 1,-1; 0, 0, 0, 0,-1, 0,-1,-1; 0, 0, 1, 1, 1, 1, 0,-1];
color = -1;

tic;
calculateNextMove(board,color,80.0);
time = toc;

executeTestPerformance('calculateNextMove_performanceBeforeEndOfMatchTest2',time);

end