function executeTestAssertEqual( functionName, result, shouldBe, time )
%% EXECUTETESTASSERTEQUAL Checks if results are equal

error = norm(result - shouldBe);
    if (error < 0.0001)
        display([functionName, ' is correct and runs ', num2str(time), ' seconds.']);
    else
        warning([functionName, ' is not correct']);
        display(shouldBe);
        display(result);
    end

end

