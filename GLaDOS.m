%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projektkurs Matlab WS 2014/2015, Lehrstuhl fuer Datenverarbeitung  %
% ================================================================== %
%                                                                    %
% Programmierprojekt Othello-Spieler                                 %
%                                                                    %
% Gruppe 1:                                                          %
% Markus Hofbauer                                                    %
% Kevin Meyer                                                        %
% Thomas Markowitz                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function board = GLaDOS(board, color, t)
%% GLaDOS (Genetic Lifeform and Disk Operating System)
% by Aperture Science
% http://en.wikipedia.org/wiki/GLaDOS
addpath(['players' filesep 'GLaDOS']);

board = calculateNextMove(board, color, t);
end