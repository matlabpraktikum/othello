function [ b_out] = JARVIS( b_in, color, t )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projektkurs Matlab SS 2014, Lehrstuhl fuer Datenverarbeitung
% ===============================================================
%
% Programmierprojekt Othello-Spieler
% 
% Gruppe B:
% Dominik Füß
% Philipp Straub
% Denis Höltje
% Andreas Gerold
% Max Blanck    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% profile on
addpath(['players' filesep 'JARVIS']);
b_out=perform_move( b_in, color, t );
% profile off
% profile viewer

%http://en.wikipedia.org/wiki/J.A.R.V.I.S.#J.A.R.V.I.S.

end

