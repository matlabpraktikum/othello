function main()
%% MAIN Calibrationfunction for the tournament hardware.
% Runs the minimax algorithm with different search depths for different
% test boards.
% Creates an file "output.txt" with the testresults.
% Output file stores KEY, SEARCHDEPTH and RUNTIME.
addpath(['..' filesep 'GLaDOS']);
filename = 'output.txt';

if exist(filename, 'file')
    delete(filename);
end

fid = fopen(filename, 'wt');
start = tic;
fprintf(fid, '  Depth\t|\t  Time\t\t|\tClock\n');
fprintf(fid, '---------------------------------------------------------\n');

testData = getTestData();
testDataLenght = size(testData,2);
for i=1:testDataLenght
    depth = testData(i).depths;
    board = testData(i).board;
    key = testData(i).key;
    color = testData(i).color;
    fprintf(fid, ' Testcase %d %s: %d\n\n',i, key, color);
    for j=1:size(depth,2)
        display([num2str(depth(j)), '..'])
        currentTime = clock;
        tic;
        minimax(color,board,depth(j),-inf,inf);
        time = toc;
        fprintf(fid, '\t%d\t|\t%f\t|\t%s\n', depth(j), time, datestr(currentTime,'HH:MM:SS.FFF'));    
    end
    fprintf(fid,'\n');
    display(['Finished Testcase ', num2str(i), ' of ', num2str(testDataLenght)])
end

fclose(fid);
display('Calibration finished successfully');
toc(start);

end

