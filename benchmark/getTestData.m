function [ testData ] = getTestData()
%% GETTESTDATA return a n x 3 cell array with the test data
% testData struct contains:
% - search depth vector
% - board
% - key

testData = struct;

testData(1).depths = 3:5;
testData(1).board = [0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0;0 0 0 1 -1 0 0 0;0 0 0 -1 1 0 0 0;0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0];
testData(1).key = 'startBoard';
testData(1).color = -1;

testData(2).depths = 3:5;
testData(2).board = [0,0, 0, 0, 0,0,0,0;0,0, 0, 0, 0,0,0,0;0,0, 0, 0, 0,0,0,0;0,0,-1,-1,-1,0,0,0;0,0, 0,-1, 1,0,0,0;0,0, 0, 0, 0,0,0,0;0,0, 0, 0, 0,0,0,0;0,0, 0, 0, 0,0,0,0];
testData(2).key = 'oneMoveMiniMax';
testData(2).color = -1;

testData(3).depths = [3:9,inf];
testData(3).board = [ 0, 1, 1, 1, 1, 1, 0,0; 0, 0, 1, 1,-1,-1,-1,1;-1, 1, 1, 1,-1,-1,-1,1;-1,-1, 1,-1,-1,-1,-1,1;-1,-1,-1, 1,-1,-1,-1,1;-1,-1,-1,-1, 1,-1, 1,1; 0, 0,-1,-1,-1, 1, 1,1; 0, 0, 1, 1, 1, 1, 1,1];
testData(3).key = 'LISA9Remaining';
testData(3).color = 1;

testData(4).depths = 3:6;
testData(4).board = [1,-1,1,1, 1, 1, 0, 0;1,-1,1,1, 1, 1, 0,-1;1,-1,1,1, 1, 1, 1,-1;1, 1,1,1, 1,-1, 1,-1;1, 1,1,1,-1, 1,-1,-1;1, 1,1,1, 1,-1, 1,-1;0, 0,1,1,-1, 0, 0, 0;0, 0,1,1, 1, 1, 0, 0];
testData(4).key = 'LISAEnd12Remaining';
testData(4).color = -1;

testData(5).depths = 3:6;
testData(5).board = [-1,-1,-1,-1,-1,-1,-1,-1;-1,-1,-1,-1,-1,-1,-1, 0; 1, 1, 1,-1,-1,-1, 1, 0; 0, 1,-1,-1,-1, 1,-1, 0; 0, 0, 1,-1,-1,-1,-1,-1; 0, 0, 1, 1,-1, 1, 1,-1; 0, 0, 1, 1, 1, 1, 1,-1; 0, 0, 0, 1, 1, 1,-1,-1];
testData(5).key = 'JARVIS13Remaining';
testData(5).color = -1;

testData(6).depths = [3:6,inf];
testData(6).board = [0,-1,-1,-1,-1,-1,0, 0;0, 1, 1,-1,-1, 1,0, 0;1, 1, 1, 1,-1,-1,1, 1;1, 1,-1,-1,-1,-1,1,-1;1, 1, 1,-1, 1, 1,1,-1;1, 1, 1, 1, 1, 1,1,-1;1, 0, 1, 1,-1, 1,0, 0;0, 1, 1, 1, 1, 1,0, 0];
testData(6).key = 'MiniMaxEnd12Remaining';
testData(6).color = -1;

testData(7).depths = 3:5;
testData(7).board = [0, 0, 1, 0, 0, 0, 0, 0; 0, 0, 1, 1, 0, 0, 0, 0; 1, 1, 1, 1, 1, 0, 0, 0; 1, 1,-1, 1, 1, 0, 0, 0; 1,-1, 1, 1,-1, 0, 0, 0;-1, 1, 1, 1, 0,-1, 0, 0; 0, 0, 0, 0, 0, 0, 0, 0; 0, 0, 0, 0, 0, 0, 0, 0];
testData(7).key = 'MiniMaxMiddle1';
testData(7).color = -1;

testData(8).depths = 3:6;
testData(8).board = [-1,-1,-1,-1,-1,-1,-1, 0; 0, 1,-1,-1,-1,-1, 0, 0; 0, 1,-1,-1,-1, 1, 1, 0; 0, 0, 1,-1,-1, 1, 1, 0; 0, 0, 1,-1,-1,-1, 1, 0; 0, 0, 1, 1,-1, 1, 1, 1; 0, 0, 1, 1, 1, 1, 1, 1; 0, 0, 0, 1, 1, 1,-1,-1];
testData(8).key = 'JARVIS19Remaining';
testData(8).color = -1;

testData(9).depths = [3:6,inf];
testData(9).board = [-1,-1,-1,-1,-1,-1, 0, 0; 0,-1, 1,-1,-1, 1, 1,-1; 1, 1,-1, 1,-1, 1, 1,-1; 1, 1,-1,-1, 1,-1, 1,-1; 1, 1, 1, 1, 1, 1, 1,-1; 1, 1, 1, 1, 1, 1, 1,-1; 1, 0, 1, 1,-1, 1, 0, 0; 0, 1, 1, 1, 1, 1, 0, 0];
testData(9).key = 'MiniMax9Remaining';
testData(9).color = -1;

testData(10).depths = [3:6,inf];
testData(10).board = [-1,-1,-1,-1,-1,-1,-1,1;-1,-1,-1,-1,-1,-1,-1,0;-1,-1, 1,-1,-1,-1, 1,1;-1, 1,-1,-1,-1, 1, 1,0;-1, 0, 1,-1,-1, 1,-1,1;-1, 0, 1, 1, 1, 1, 1,1; 0, 0, 1, 1, 1, 1, 1,1; 0, 0, 0, 1, 1, 1,-1,1];
testData(10).key = 'JARVISEnd9Remaining';
testData(10).color = -1;

testData(11).depths = 3:5;
testData(11).board = [-1, 0, 0,-1, 0, 1, 0,0; 0,-1,-1,-1, 1, 1, 0,0; 0, 1,-1,-1, 1, 1, 1,0; 0, 0, 1,-1,-1,-1, 1,0; 0, 0, 1,-1,-1,-1,-1,0; 0, 0, 1,-1,-1,-1, 0,0; 0, 0, 0, 0,-1, 0, 0,0; 0, 0, 0, 0, 0, 0, 0,0];
testData(11).key = 'JARVISMiddle1';
testData(11).color = -1;

testData(12).depths = 3:5;
testData(12).board = [-1, 0, 0,-1, 0, 1, 0,0; 0,-1,-1,-1, 1, 1, 0,0; 0, 1,-1, 1,-1,-1, 0,0; 0, 0, 1, 1,-1,-1, 1,0; 0, 0, 1, 1,-1,-1,-1,0; 0, 0, 1, 0,-1,-1, 0,0; 0, 0, 0, 0,-1, 0, 0,0; 0, 0, 0, 0, 0, 0, 0,0];
testData(12).key = 'JARVISMiddle2';
testData(12).color = -1;

testData(13).depths = 3:5;
testData(13).board = [0, 0, 0,-1, 0, 0, 0,0; 0, 0,-1,-1, 0, 0, 0,0; 0, 1,-1,-1,-1,-1, 0,0; 0, 0, 1, 1, 1,-1, 1,0; 0, 0, 1, 1,-1,-1,-1,0; 0, 0, 1, 0, 1,-1, 0,0; 0, 0, 0, 0, 0, 0, 0,0; 0, 0, 0, 0, 0, 0, 0,0];
testData(13).key = 'JARVISMiddle3';
testData(13).color = -1;

testData(14).depths = 3:5;
testData(14).board = [0, 0, 0, 0, 0, 0, 0,0; 0, 0, 0, 0, 0, 0, 0,0; 0, 0, 0, 0, 1, 0, 0,0; 0, 0,-1,-1, 1, 0, 0,0; 0, 0, 0,-1,-1, 0, 0,0; 0, 0, 0, 0, 0,-1, 0,0; 0, 0, 0, 0, 0, 0, 0,0; 0, 0, 0, 0, 0, 0, 0,0];
testData(14).key = 'JARVISMiddle4';
testData(14).color = -1;

testData(15).depths = [3:6, inf];
testData(15).board = [0, 0, 0, 0, 0,-1, 1, 1; 0, 0, 0, 1,-1,-1,-1,-1;-1,-1,-1,-1, 1,-1,-1,-1;-1,-1,-1, 1, 1,-1,-1,-1;-1,-1, 1, 1,-1, 1,-1,-1;-1, 1, 1,-1,-1,-1,-1,-1; 1, 1, 1, 1, 1,-1,-1,-1; 0, 0,-1,-1,-1,-1,-1,-1];
testData(15).key = 'LISAEnd10Remaining';
testData(15).color = 1;

testData(16).depths = [3:6, inf];
testData(16).board = [0, 0, 0, 0, 0, 0, 1, 1; 0, 0, 0, 1, 1, 1, 1,-1;-1,-1,-1, 1, 1, 1,-1,-1;-1,-1,-1,-1,-1,-1,-1,-1;-1,-1,-1,-1,-1, 1,-1,-1;-1,-1,-1,-1,-1,-1,-1,-1; 0, 0,-1, 1, 1,-1,-1,-1; 0, 0,-1,-1,-1,-1,-1,-1];
testData(16).key = 'LISAEnd13Remaining';
testData(16).color = 1;

testData(17).depths = [3:7, inf];
testData(17).board = [0, 0, 0,-1, 0,-1, 1, 1; 0, 0, 0,-1,-1,-1,-1,-1;-1,-1,-1,-1, 1,-1,-1,-1;-1,-1,-1, 1,-1,-1,-1,-1;-1,-1, 1,-1,-1, 1,-1,-1;-1, 1,-1,-1,-1,-1,-1,-1;-1, 1, 1, 1, 1,-1,-1,-1;-1, 1,-1,-1,-1,-1,-1,-1];
testData(17).key = 'LISAEnd7Remaining';
testData(17).color = 1;

testData(18).depths = 3:5;
testData(18).board = [0, 0, 1, 1, 1, 0, 0, 0; 0, 0, 1, 1, 1, 1, 0, 0; 1, 1, 1,-1,-1,-1, 0, 0; 1, 1,-1, 1, 1, 0, 0, 0; 1,-1, 1, 1, 1, 0, 0, 0;-1, 1, 1, 1, 1,-1, 0, 0; 0, 0, 0, 0, 0, 0, 0, 0; 0, 0, 0, 0, 0, 0, 0, 0];
testData(18).key = 'MiniMaxMiddle2';
testData(18).color = -1;

testData(19).depths = [3:8, inf];
testData(19).board = [1,-1, 1, 1, 1, 1, 0, 1;1,-1,-1,-1,-1,-1, 1,-1;1,-1, 1,-1, 1, 1,-1,-1;1, 1, 1, 1, 1, 1,-1,-1;1, 1, 1, 1,-1, 1,-1,-1;1, 1, 1, 1, 1, 1,-1,-1;0, 0, 1, 1, 1, 1, 0,-1;0, 0, 1, 1, 1, 1, 0, 0];
testData(19).key = 'LISAEnd8Remaining';
testData(19).color = -1;

testData(20).depths = 3:6;
testData(20).board = [-1,-1,-1,-1,-1, 1, 0, 0; 0, 1,-1,-1,-1, 1, 0, 0; 0, 1,-1,-1,-1, 1, 1, 0; 0, 0, 1,-1,-1, 1, 1, 0; 0, 0, 1,-1,-1,-1, 1, 0; 0, 0, 1, 1,-1, 1, 1, 1; 0, 0, 1, 1, 1,-1,-1, 0; 0, 0, 0, 1, 1, 1,-1,-1];
testData(20).key = 'JARVIS21Remaining';
testData(20).color = -1;

end
