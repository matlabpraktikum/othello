%% Running Tournament in git-ignored players directory.

cleanTournament;

playersFolder = 'players';
playerName = 'GLaDOS';
enemyFolder = 'test_enemys';
logFolder = 'logs';

% removing old "build"/"run" folder and create a new one
if exist(playersFolder, 'dir')
    rmdir(playersFolder, 's');
end
mkdir(playersFolder);

%% Copying our player to the run folder
copyfile([playerName, '*'], playersFolder);

% OwnPlayer2
% copyfile([playerName, '.m'], [playersFolder, filesep, playerName, '2.m']);

%% Selecting Enemy
% Uncomment the players you would like to test.

% LDV's MiniMax-Player
% copyfile([enemyFolder, filesep ,'MiniMaxPlayer*'], playersFolder);

% Old player (JARVIS)
% copyfile([enemyFolder, filesep ,'JARVIS*'], playersFolder);

% Last year's winner (Lisa)
copyfile([enemyFolder, filesep ,'Lisa*'], playersFolder);


%% Starting the tournament
tournament_main('time_budget',180,'games_per_pair', 2)
