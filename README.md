# Projektpraktikum Matlab

## Othello/Reversi-Projekt, Gruppe 1, WS 2014/2015

This is a project of students of *Electrical Engineering and Information Technology* at [Technical University of Munich](http://www.tum.de) (TUM).  
This project is part of [LDV's](http://www.ldv.ei.tum.de) course [Projektpraktikum Matlab](http://www.ldv.ei.tum.de/lehre/projektpraktikum-matlab/).

**Written by:**

-   Markus Hofbauer
-   Kevin Meyer
-   Thomas Markowitz

## Player: GLaDOS (by Aperture Science)

Othello/Reversi Computer Player working with MiniMax-Algorithm (optimised by alpha-beta pruning).  
![5643874030_530d7eba22.jpg](https://bitbucket.org/repo/6nkpqn/images/2262407377-5643874030_530d7eba22.jpg)

Check out also the preparing [homework](https://bitbucket.org/matlabpraktikum/matlab-ha).