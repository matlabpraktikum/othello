%% Testrunner
% calls all testfunctions in source directory.
% We have to clean the Tournament first to be sure to test the code from
% the correct folder.
cleanTournament();

addpath('GLaDOS');
disp('Starting Testrunner');

getPossibleMovesTest();
minimaxTest();
calculateWeightTest();
getSearchDepthTest();
calculateNextMoveTest();

disp(' ')
disp('Testrunner finished');
